import { forwardRef, useState } from 'react';
import './Input.css';

export const Input = forwardRef(({ value, onChange }, ref) => {
    

    const handleChange = (e) => {
        onChange(e.target.value);
    }

    return (
        <div className="Input">
            <input value={value} onChange={handleChange} ref={ref}/>
        </div>
    )
})