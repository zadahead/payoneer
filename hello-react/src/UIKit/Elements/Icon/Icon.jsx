export const Icon = (props) => {
    return (
        <span className="material-symbols-rounded">{props.i}</span>
    )
}

export default Icon;