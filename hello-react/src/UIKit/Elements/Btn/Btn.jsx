 import './Btn.css';

export const Btn = (props) => {

    return (
        <button className="Btn" onClick={props.onClick}>{props.children}</button>
    )
 }

 export const SpecialBtn = () => {
    return (
        <button>Special</button>
    )
 }

 export default Btn;