import { useEffect, useState } from 'react';
import { Between, Rows } from 'UIKit';
import Icon from '../Icon/Icon';
import './Dropdown.css';

export const Dropdown = ({ list, selected, def, onChange }) => {
    const [isOpen, setIsOpen] = useState(false);

    useEffect(() => {
        document.body.addEventListener('click', hideItems);

        return () => {
            document.body.removeEventListener('click', hideItems);
        }
    }, []);

    const hideItems = () => {
        console.log('hideItems');
        setIsOpen(false);
    }

    const handleToggle = (e) => {
        e.stopPropagation();
        setIsOpen(!isOpen);
    }

    const handleSelect = (i) => {
        console.log('item => ', i);
        onChange(i.id);
        setIsOpen(false);
    }

    const renderTitle = () => {
        if (selected) {
            const item = list.find(i => i.id === selected);
            if (item) {
                return item.value;
            }
        }
        return def || 'Please Select';
    }
    const renderList = () => {
        return list.map(i => {
            const className = i.id === selected ? 'selected' : '';
            return (
                <h4
                    key={i.id}
                    onClick={() => handleSelect(i)}
                    className={className}
                >
                    {i.value}</h4>
            )
        })
    }

    return (
        <div className="Dropdown">
            <div className='header' onClick={handleToggle}>
                <Between>
                    <h3>{renderTitle()}</h3>
                    <Icon i="expand_more" />
                </Between>
            </div>
            {isOpen && (
                <div className='items'>
                    {renderList()}
                </div>
            )}
        </div>
    )
}