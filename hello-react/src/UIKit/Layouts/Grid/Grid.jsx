import './Grid.css';

export const Grid = (props) => {
    return (
        <div className="Grid">
            <div>
                {props.children[0]}
            </div>
            <div>
                {props.children[1]}
            </div>
        </div>

    )
}

export default Grid;