import './Line.css';

export const Line = (props) => {
    const getClassName = () => {
        const className = 'Line ' + (props.type || '');
        return className.trim();
    }
    return (
        <div className={getClassName()}>
            {props.children}
        </div>
    )
}

export const Rows = (props) => {
    return <Line {...props} type="rows" />
}

export const Between = (props) => {
    return <Line {...props} type="between" />
}

export default Line;