import { useState, useEffect } from "react"
import { Btn, Line } from "UIKit"

export const Counter = () => {
    const [color, setColor] = useState('');
    const [count, setCount] = useState(0);

    useEffect(() => {
        setTimeout(() => {
            setColor('red');
            setCount(10);
        }, 500);
    }, [])

    useEffect(() => {
        console.log('Mounted');

        document.body.addEventListener('click', handleBodyClick);

        return () => {
            console.log('unmounted');
            document.body.removeEventListener('click', handleBodyClick);
        }
    }, [])

    const handleBodyClick = () => {
        console.log('page clicked!');
    }

    useEffect(() => {
       // console.log('Mounted => Updated');
       
    })

    useEffect(() => {
        console.log('count has updated', count);
        return () => {
            console.log('count is about to change', count)
        }
    }, [count])

    const handleChange = () => {
        setColor(color === 'red' ? 'blue' : 'red');
    }
    const handleAdd = () => {
        setCount(count + 1);
    }

    const styleCss = {
        color: color
    }

    return (
       <div>
         <Line>
            <h2 style={styleCss}>Count, {count}</h2>
            <Btn onClick={handleAdd}>Add</Btn>
            <Btn onClick={handleChange}>Change</Btn>
         </Line>
       </div>
    )
}