import { useState } from "react"
import { Btn } from "UIKit"

export const ColorSwitch = () => {
    const [color, setColor] = useState('red');

    const handleSwitch = () => {
        setColor(color === 'red' ? 'blue' : 'red');
    }

    const styleCss = {
        width: '50px',
        height: '50px',
        backgroundColor: color
    }

    return (
        <div>
           <div style={styleCss}></div>
           <Btn onClick={handleSwitch}>Switch</Btn> 
        </div>
    )
}