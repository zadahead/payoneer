export const User = ({ id, name, age}) => {
    return (
        <div>
            <h4> #{id} name: {name} - age ({age})</h4>
        </div>
    )
}