import { useState } from "react"
import { Btn } from "UIKit"

export const Toggler = ({ element }) => {
    const [isShow, setIsShow] = useState(true);

    const handleToggle = () => {
        setIsShow(!isShow);
    }

    return (
        <div>
            <h3>Toggler</h3>
            <Btn onClick={handleToggle}>Toggle</Btn>
            {isShow && element}
        </div>
    )
}