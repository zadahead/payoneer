const SayHi = (props) => {
    console.log(props);

    const styleCss = {
        backgroundColor: 'red'
    }

    return (
        <div>
            <span>Hey {props.name}</span>
            <div style={styleCss}>
                {props.children}
            </div>
        </div>
    )
}

/*
    <Form /> =>
*/

export default SayHi;