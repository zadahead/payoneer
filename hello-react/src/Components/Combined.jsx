import { useState } from "react"
import { Btn, Line } from "UIKit"

export const Combined = () => {
    const [count, setCount] = useState(0);
    const [color, setColor] = useState('red');
    console.log('render', count, color);

    const double = () => {
        handleAdd();
        handleSwitch();
    }

    const handleAdd = () => {
        setCount(count + 1);
    }

    const handleSwitch = () => {
        setColor(color === 'red' ? 'blue' : 'red');
    }

    const styleCss = {
        color: color
    }

    return (
        <div>
            <Line>
                <h2 style={styleCss}>Count, {count}</h2>
                <Btn onClick={handleAdd}>Add</Btn>
                <Btn onClick={handleSwitch}>Switch</Btn>
                <Btn onClick={double}>double</Btn>
            </Line>
        </div>
    )
}