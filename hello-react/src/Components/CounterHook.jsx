import { useCounter } from "Hooks/useCounter";
import { Btn, Line } from "UIKit"

/*
    1) create a fully working component
    2) separate between logic and render
    3) create custom hook, and move logic 
    4) recognize what values the render needs, return them from the custom hook
    5) call the custom hook from render component, and distruct the values.  
    6) move custom hook to a shared folder and export it
*/



export const CounterHook = () => {
    //logic
    const { count, handleAdd } = useCounter();

    //render
    return (
        <div>
            <Line>
                <h2>CounterHook, {count}</h2>
                <Btn onClick={handleAdd}>Add</Btn>
            </Line>
        </div>
    )
}