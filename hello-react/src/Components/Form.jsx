const Form = (props) => {

    const styleCss = {
        backgroundColor: '#ddd',
        padding: '10px'
    }

    const boxStyle = {
        border: '1px solid #333',
        padding: '10px',
        textAlign: 'center',
        margin: '10px'
    }


    return (
        <div style={boxStyle}>
            <h1>{props.headline}</h1>
            <h2>{props.info}</h2>
            <div style={styleCss}>
                {props.children}
            </div>
        </div>
    )
}

export default Form;