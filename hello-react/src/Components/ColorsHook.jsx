import { useColorSwitch } from "Hooks/useColorSwitch";
import { Btn, Line } from "UIKit"


export const ColorsHook = () => {
    //logic
    const { color, handleSwitch } = useColorSwitch();

    //render
    const styleCss = {
        color
    }
    return (
        <div>
            <Line>
                <h4 style={styleCss}>Colors</h4>
                <Btn onClick={handleSwitch}>Change Color</Btn>
            </Line>
        </div>
    )
}