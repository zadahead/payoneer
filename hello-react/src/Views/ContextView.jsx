import { welcomeContext } from "Context/welcomeContext"
import { useContext } from "react"
import { Btn, Rows } from "UIKit"

export const ContextView = () => {
    const { count, setCount } = useContext(welcomeContext);
    const handleAdd = () => {
        setCount(count + 1);
    }

    return (
        <Rows>
            <h1>Context view</h1>
            <h4>{count}</h4>
            <Btn onClick={handleAdd}>Add</Btn>
        </Rows>
    )
}