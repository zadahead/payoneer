import { User } from "Components/User"
import { useState } from "react"
import { Btn, Line, Rows, Input } from "UIKit"

const users = [
    { id: 1, name: 'mosh', age: 25 },
    { id: 2, name: 'david', age: 35 },
    { id: 3, name: 'anat', age: 28 }
]

/*
 <User /> => #{id} name: {name} - age ({age})

 #1 name: mosh - age (25)
*/

export const UsersViews = () => {
    const [name, setName] = useState('');
    const [age, setAge] = useState('');

    const [list, setList] = useState(users)

    const handleAddUser = () => {
        list.push({ id: list.length + 1, name: name, age: age})
        console.log(list);
        setList([...list]);

        setName('');
        setAge('');
    }
    
    const renderList = () => {
        return list.map((u) => {
            return <User key={u.id} {...u} />
        })
    }

    return (
        <Rows>
            <h1>Users View</h1>
            <Line>
                <Input value={name} onChange={setName} />
                <Input value={age} onChange={setAge} />
                <Btn onClick={handleAddUser}>Add User</Btn>
            </Line>
            <Rows>
                {renderList()}
            </Rows>
        </Rows>
    )
}