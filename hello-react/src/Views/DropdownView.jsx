import { useState } from "react"
import { Dropdown, Rows } from "UIKit"

const list = [
    { id: 1, value: 'item 1'},
    { id: 2, value: 'item 2'},
    { id: 3, value: 'item 3'},
    { id: 4, value: 'item 4'},
    { id: 5, value: 'item 5'}
]
export const DropdownView = () => {
    const [selected, setSelected] = useState(null);

    return (
        <Rows>
            <h1>Dropdown</h1>
            <Dropdown list={list} selected={selected} onChange={setSelected} />
            <h3>asdas</h3>
        </Rows>
    )
}