import { Counter } from "Components/Couter"
import { Toggler } from "Components/Toggler"
import { Rows } from "UIKit"

export const LifeCycleView = () => {
    return (
        <Rows>
            <h1>Life Cycle - useEffect</h1>
            <Toggler element={<Counter />} />
        </Rows>

    )
}