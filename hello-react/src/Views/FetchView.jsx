import axios from "axios";
import { useEffect, useState } from "react";
import { Rows } from "UIKit";

import Loading from 'Images/Pulse-1s-200px.svg';

const useFetch = (path) => {
    const [isLoading, setIsLoading] = useState(true);
    const [error, setError] = useState('');
    const [list, setList] = useState([]);

    useEffect(() => {
        axios.get(`https://jsonplaceholder.typicode.com${path}`)
            .then((resp) => {
                setTimeout(() => {
                    setList(resp.data);
                    setIsLoading(false);
                }, 500)
            })
            .catch((err) => {
                console.log('ERROR => ', err.message);
                setError(err.message);
            })
    }, [])

    return {
        list,
        setList,
        isLoading,
        error
    }
}

const Fetch = ({ path, onRender }) => {
    //logic
    const { list, setList, isLoading, error } = useFetch(path);

    //render 

    const renderList = () => {
        return onRender(list);
    }

    const styleCss = {
        width: '30px'
    }


    const renderView = () => {
        if (error) {
            const errStyle = {
                color: 'red'
            }
            return (
                <h4 style={errStyle}>{error}</h4>
            )
        }

        if(isLoading) {
            return <img style={styleCss} src={Loading} />
        }

        return renderList()
    }

    return renderView();
}

const FetchUsers = () => {
    const renderList = (list) => {
        return list.map(u => {
            return <h3 key={u.id}>{u.name}</h3>
        })
    }

    return <Fetch path="/users" onRender={renderList} />
}

const FetchTodos = () => {
    const renderList = (list) => {
        return list.map(i => {
            const styleCss = {
                opacity: i.completed ? '0.3' : '1'
            }
            return (
                <h4
                    key={i.id}
                    style={styleCss}
                >
                    {i.title}
                </h4>
            )
        })
    }
    return <Fetch path="/todos" onRender={renderList} />
}

export const FetchView = () => {

    return (
        <Rows>
            <h1>Fetch View</h1>
            <FetchUsers />
            <FetchTodos />
        </Rows>
    )
}