import { ColorsHook } from "Components/ColorsHook"
import { CounterHook } from "Components/CounterHook"
import { Rows } from "UIKit"


export const HooksView = () => {
    return (
        <Rows>
            <h1>Custom Hooks View</h1>
            <ColorsHook />
            <CounterHook />
        </Rows>
    )
}