import { useEffect, useReducer, useRef, useState } from "react"
import { Btn, Input, Rows } from "UIKit"

const ref = {
    count: 0
}

export const RefView = () => {
    const [count, setCount] = useState(0);
    const ref = useRef();
    ref.current = count;

    const inputref = useRef();

    console.log('count => ', count);

    useEffect(() => {
        inputref.current.style.backgroundColor = 'red';

        document.body.addEventListener('click', showCount);

        setTimeout(() => {
            console.log('timeout count => ', ref.current)
        }, 5000)
    }, [])

    const showCount = () => {
        console.log('body count => ', ref.current);
    }
    const handleAdd = () => {
        setCount(count + 1);
    }

    return (
        <Rows>
            <h1>useRef, {count}</h1>
            <Input  ref={inputref} />
            <Btn onClick={handleAdd}>Add</Btn>
        </Rows>
    )
}

export const RefViewWrapper = () => {
    return (
        <div>
            <RefView />
            <RefView />
        </div>
    )
}