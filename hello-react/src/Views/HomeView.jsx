import { Line, Icon, Btn } from 'UIKit';

export const HomeView = () => {
    const handleClick = (e) => {
        console.log('Clicked!', e);
    }
    
    return (
        <div>
            <Line>
                <Icon i="home" />
                <Icon i="settings" />
                <h3>Content</h3>
                <h4>More content</h4>
                <Btn onClick={handleClick}>Click ME</Btn>
            </Line>
        </div>
    )
}