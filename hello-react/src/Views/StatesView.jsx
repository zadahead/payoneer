import { ColorSwitch } from "Components/ColorSwitch"
import { Combined } from "Components/Combined"
import { Counter } from "Components/Couter"
import { Toggler } from "Components/Toggler";
import { useState } from "react";
import { Btn, Input, Rows } from "UIKit";


export const StatesView = () => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    const handleLogin = () => {
        console.log('LOGIN', username, password);
    }

    return (
        <div>
            <h1>States View</h1>
            <Rows>
                <Input value={username} onChange={setUsername} />
                <Input value={password} onChange={setPassword} />
                <Btn onClick={handleLogin}>Login</Btn>
            </Rows>
        </div>
    )
}