import { createContext, useState } from "react";

export const welcomeContext = createContext('');

const Provider = welcomeContext.Provider;

export const WelcomeProvider = ({ children }) => {
    const [count, setCount] = useState(0);

    const value = {
        count,
        setCount
    }

    return (
        <Provider value={value}>
            {children}
        </Provider>
    )
}