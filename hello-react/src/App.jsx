import { Route, Routes, Link, NavLink } from 'react-router-dom';
import './App.css';


import { Grid, Line } from 'UIKit';
import { StatesView } from 'Views/StatesView';
import { HomeView } from 'Views/HomeView';
import { UsersViews } from 'Views/UsersView';
import { LifeCycleView } from 'Views/LifeCycleView';
import { FetchView } from 'Views/FetchView';
import { DropdownView } from 'Views/DropdownView';
import { RefView, RefViewWrapper } from 'Views/RefView';
import { HooksView } from 'Views/HooksView';
import { ContextView } from 'Views/ContextView';
import { useContext } from 'react';
import { welcomeContext } from 'Context/welcomeContext';


const App = () => {
    const { count } = useContext(welcomeContext);


    return (
        <div className="App">
            <Grid>
                <Line>
                    <NavLink to="/home">Home</NavLink>
                    <NavLink to='/states'>States</NavLink>
                    <NavLink to='/users'>Users</NavLink>
                    <NavLink to='/cycle'>Cycle</NavLink>
                    <NavLink to='/fetch'>Fetch</NavLink>
                    <NavLink to='/dropdown'>Dropdown</NavLink>
                    <NavLink to='/ref'>Ref</NavLink>
                    <NavLink to='/hooks'>Hooks</NavLink>
                    <NavLink to='/context'>Context</NavLink>
                    <h4>{count}</h4>
                </Line>
                <div>
                    <div>
                        <Routes>
                            <Route path='/home' element={<HomeView />} />
                            <Route path='/states' element={<StatesView />} />
                            <Route path='/users' element={<UsersViews />} />
                            <Route path='/cycle' element={<LifeCycleView />} />
                            <Route path='/fetch' element={<FetchView />}/>
                            <Route path='/dropdown' element={<DropdownView />} />
                            <Route path='/ref' element={<RefViewWrapper />} />
                            <Route path='/hooks' element={<HooksView />} />
                            <Route path='/context' element={<ContextView />} />
                        </Routes>
                    </div>
                </div>
            </Grid>
        </div>
    )
}

export default App;